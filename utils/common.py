from time import sleep, time


def wait(condition, timeout=10, interval=2, err_msg=None):
    """
    Функция ожидает пока condition не станет True,
    иначе кидает исключение по таймауту
    """
    raise_time = time() + timeout
    while time() < raise_time:
        if condition():
            return
        sleep(interval)
    raise TimeoutError(err_msg)

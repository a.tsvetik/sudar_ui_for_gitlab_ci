from os import path, makedirs
from time import strftime, gmtime
from threading import Lock

from pytest import fixture, hookimpl
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from page.base_page import BasePage
from page.main_page import MainPage
from page.sing_in_popap import SignInPopap
from settings.manager import manager


lock = Lock()


def pytest_addoption(parser):
    """Передача значений параметров из командной строки"""
    for opt in vars(manager.options).values():
        parser.addoption(opt.name, action=opt.action,
                         default=opt.default, help=opt.info)


def pytest_make_parametrize_id(val):
    """Чтобы русские тестовые данные отображались корректно в ходе теста"""
    return repr(val)


def pytest_sessionstart(session):
    """Падготавливаем данные"""
    # Добавляем папку с для хранения результатов
    if not path.exists(manager.paths.results):
        makedirs(manager.paths.results)
    # Присваиваем значения всем кастомным ключам
    for opt_name in vars(manager.options).keys():
        arg_value = getattr(session.config.option, opt_name)
        setattr(getattr(manager.options, opt_name), 'value', arg_value)
    # Парсим конфиги на основе кастомных ключей
    manager.set_configs()


def pytest_runtest_setup(item):
    """Получить имя класса теста и время его запуска"""
    manager.test_name = item.name or item.originalname
    manager.test_start = strftime("%d-%m-%Y_%H-%M")


@hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item):
    """Хук для перехвата статуса тест кейса"""
    outcome = yield
    lock.acquire()
    result = outcome.get_result()
    # rep_{result.when} используется в фикстуре kill_driver()
    setattr(item, f'rep_{result.when}', result)
    lock.release()


# @fixture()
# def driver():
#     driver = webdriver.Chrome(ChromeDriverManager().install())
#     # driver.set_window_size(1920, 1080)
#     driver.maximize_window()
#     yield driver
#     driver.quit()

@fixture()
def driver(request):
    br_name = manager.options.browser_name.value
    br_size = manager.options.browser_size.value
    selenoid_cfg = manager.selenoid
    capabilities = {
        'browserName': br_name,
        'browserVersion': selenoid_cfg['version'],
        'selenoid:options': {
            'enableVNC': selenoid_cfg['vnc'],
            'enableVideo': selenoid_cfg['video'],
            'name': manager.test_name
        }
    }
    chrome_options = Options()
    chrome_options.add_argument('--disable-dev-shm-usage')
    driver = webdriver.Remote(
        command_executor=selenoid_cfg['url'],
        desired_capabilities=capabilities,
        options=chrome_options)
    res_width, res_height = br_size.split('x')
    driver.set_window_size(res_width, res_height)

    def kill_driver():
        scr_dir = path.join(manager.paths.results, 'screenshots')
        if not path.exists(scr_dir):
            makedirs(scr_dir)
        if request.node.rep_setup.failed or request.node.rep_call.failed:
            scr_name = f'{manager.test_start}_{manager.test_name}.png'
            scr_dir = path.join(manager.paths.results, 'screenshots')
            driver.save_screenshot(path.join(scr_dir, scr_name))
        driver.quit()
    request.addfinalizer(kill_driver)
    return driver

# @pytest.mark.tryfirst
# def pytest_runtest_makereport(item, call, __multicall__):
#     rep = __multicall__.execute()
#     setattr(item, "rep_" + rep.when, rep)
#     return rep
#
# @pytest.fixture(scope="function")
# def screenshot_on_failure(request):
#     def fin():
#         driver = SeleniumWrapper().driver
#         attach = driver.get_screenshot_as_png()
#         if request.node.rep_setup.failed:
#             allure.attach(request.function.__name__, attach, allure.attach_type.PNG)
#         elif request.node.rep_setup.passed:
#             if request.node.rep_call.failed:
#                 allure.attach(request.function.__name__, attach, allure.attach_type.PNG)
#     request.addfinalizer(fin)



@fixture()
def main_page(driver):
    """Переходит на 'Главную страницу' и возвращает ее инстанс"""
    page = BasePage(driver)
    page.go()
    page.wait_header_area()
    page.wait_for_url_change_to_expected()
    return page


@fixture()
def main_page_user(request, main_page, driver):
    """Переход на 'Главную страницу' с кредами юзера полученных из маркера 'auth_user'"""
    user_name = request.node.get_closest_marker('auth_user').args[0]
    user_info = manager.users[user_name]
    sign_in_popap = SignInPopap(main_page.driver)
    sign_in_popap.sign_in(user_info['email'], user_info['password'])
    main_page = MainPage(main_page.driver)
    main_page.wait_profile_name_button()
    return main_page

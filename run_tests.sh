#!/bin/bash
cd /var/api_billing/ 2>/dev/null

env=$1
selection=$2
NAMESPACE=$3
run_alias="ui_tests_"$NAMESPACE

number='^[0-9]+$'
if [[ $4 =~ $number ]]; then
  maxfail=$4
  if [[ $# -eq 6 || ($# -eq 5 && $5 != "y") ]]; then
    testfile=$5
    develop_run=$6
    tests=`echo $testfile | sed -r "s/,/\n/g" | awk '{print "tests/"$0}' | xargs`
  elif [ $# -eq 5 ]; then
    develop_run=$5
  fi
else
  maxfail=20
  if [[ $# -eq 5 || ($# -eq 4 && $4 != "y") ]]; then
    testfile=$4
    develop_run=$5
    tests=`echo $testfile | sed -r "s/,/\n/g" | awk '{print "tests/"$0}' | xargs`
  elif [ $# -eq 4 ]; then
    develop_run=$4
  fi
fi

echo "Config files: "
echo "* ENV: $env"
echo "* NAMESPACE: $NAMESPACE"
echo "* Selection config: $selection"
echo "* Test file: $testfile"
echo "* Maximum fails: $maxfail"
echo "* Develop run: $develop_run"
echo

pytest --alias=${run_alias} \
       --env=${env} \
       --namespace=${NAMESPACE} \
       --chat-name=reports \
       --send-reports=y \
       --br-name=chrome \
       --reruns 2 --reruns-delay 3 \
       --selenoid=server \
       --selection=${selection} \
       --maxfail=${maxfail} \
       --develop-run=${develop_run} \
       -n 10 \
       ${tests}
import datetime
from os import getcwd, path, makedirs, listdir

# from PIL import Image
from yaml import full_load as yaml_load


# from PIL import Image
# from yaml import dump as yaml_dump
# from requests import get as req_get
# from urllib.parse import urljoin
# from time import time
# from json import loads as json_loads
# from zipfile import ZipFile, ZIP_DEFLATED
# import framestart


class Paths:

    def __init__(self):
        self.configs = ''
        self.results = ''
        self.set_paths()

    def set_paths(self):
        """Определить путь к файлу"""
        for f in self.__dict__.keys():
            if f == 'results':
                setattr(self, f, path.join(getcwd(), f))
            else:
                setattr(self, f, path.join(path.dirname(__file__), f))


class FileNames:
    SELENOID = 'selenoid.yml'
    ENV = 'env.yml'
    USERS = 'users.yml'
    # PAGES = 'pages.yml'


class Option:
    def __init__(self, name, action='store', default='', info=''):
        self.name = name
        self.action = action
        self.default = default
        self.info = info
        self.value = ''


class Options:
    def __init__(self):
        """
        Имена опций должны соответствовать преобразованию:
        --option-name = option_name
        То есть два первых дефиса будут убраны, а последующие будут преобразованы в нижнее подчеркивание.
        Именно так делает преобразование имен опций PyTest
        """
        self.selenoid = Option(
            '--selenoid', default='server',
            info='Name of selenoid host from config file'
                 '(local, server)')
        self.browser_name = Option(
            '--browser-name', default='chrome',
            info='Browser name for UI testing')
        self.browser_size = Option(
            '--browser-size', default='1920x1080',
            info='Window size of browser for UI testing')
        self.env = Option(
            '--env', default='test', info='Environment name')
        self.alias = Option(
            '--alias', default='TEST_RUN',
            info='Alias for test run')
        self.pages = Option(
            '--pages', default='pages.yml',
            info='Filename of pages config for Page Objects')


class PyTestManager:
    paths = Paths()
    file_names = FileNames()
    options = Options()
    test_name = ''
    test_start = ''

    def __init__(self):
        self.selenoid = {}
        self.env = {}
        self.users = {}
        # self.pages = {}

    def __parse_yml(self, filename):
        """Распарсить yml файл"""
        if filename:
            return yaml_load(open(path.join(self.paths.configs, filename)))
        return dict()

    def set_configs(self):
        """Записываем полученную информацию из yml файла"""
        self.selenoid = self.__parse_yml(self.file_names.SELENOID).get(self.options.selenoid.value)
        self.env = self.__parse_yml(self.file_names.ENV).get(self.options.env.default)
        self.users = self.__parse_yml(self.file_names.USERS)


manager = PyTestManager()

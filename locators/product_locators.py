"""Локаторы страницы 'Продукт'"""

# Кнопка 'Добавить в корзину'
add_in_basket_button = '//button[contains(@class,"cart-favour")]'

# # Выпадающий список 'Размеры'
# size_drop_down = '//div[contains(@class,"sizes-chooser-item")]'

# Кнопка 'Добавлено'
added_in_basket_button = '//button[contains(@class,"cart-favour") and @data-status="added"]'

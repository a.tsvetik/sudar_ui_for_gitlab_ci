FROM python:3.10.6-slim-buster

COPY . sudar/

WORKDIR sudar

ENV TZ=Europe/Moscow

RUN apt-get update -y && apt-get install -y python3-pip && apt-get install -yy tzdata

RUN cp /usr/share/zoneinfo/$TZ /etc/localtime

RUN pip install -r requirements.txt

VOLUME ["/sudar/results"]

CMD  ["sh", "-c", "python3 -m pytest tests/test_product.py --alluredir=results/allure_results"]
